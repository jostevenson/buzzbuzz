# Buzzer App

## Overview

The buzzers are intended to be used with a driver and app suite that the manufacturer publishes ( [see docs](https://teams.microsoft.com/_#/files/General?threadId=19:6f5878ad7f134407b593018013607330@thread.skype&amp;ctx=channel&amp;context=Code%252FBuzzer%2520Docs%2520(useless))).  Only, this was useless for our purposes, since we couldn&#39;t really get cheesy game show software to drive the Eagle&#39;s Nest pitch sessions.

So, Al designed a [web-based UI](https://teams.microsoft.com/_#/pdf/viewer/teams/https%3A~2F~2Favanade.sharepoint.com~2Fsites~2FExpressInnovationTeam~2FShared%20Documents~2FGeneral~2FCode~2FButton%20Screen_V0.pdf?threadId=19%3A6f5878ad7f134407b593018013607330%40thread.skype&amp;baseUrl=https%3A~2F~2Favanade.sharepoint.com~2Fsites~2FExpressInnovationTeam&amp;fileId=291B4DCB-9361-4251-91C6-EEA3720A91FC&amp;ctx=files&amp;viewerAction=view), and I built a little app to take in the buzzes (note: the needs of this little apps grew and changed unexpectedly … I make no enterprise-grade claims to quality code 😉)

## Presenting

While presenting, I would run the BuzzBuzz desktop form and the PowerPoint presentation manager on my laptop screen and the BuzzBuzzUI website and PowerPoint presentation (both in full screen mode) on the projector screen.

**Note** _: make sure that the Debug chrome window with the website and the current PowerPoint pitch are the only chrome and PowerPoint windows or the_ _ToggleForegroundProcess_ _logic won&#39;t work._

## BuzzBuz.sln

The projects were all built and run using VS2017 (v15.6.4).

- Debug with two startup projects: BuzzBuzz and BuzzBuzzUI
- Even during the pitch session, I ran the project directly out of VS in debug mode (eeck!).  The reason is that there are [severe limitations](https://msdn.microsoft.com/en-us/library/windows/desktop/ms633539%28v=vs.85%29.aspx?f=255&amp;MSPPError=-2147217396) on which processes can switch the foreground process.  Perhaps there are [better solutions](https://stackoverflow.com/questions/2315561/correct-way-in-net-to-switch-the-focus-to-another-application).

### USBHIDDRIVER

Found this project [online](http://www.florian-leitner.de) to handle communication to/from the buzzers:

- This project was basically a black box to me.
- Its functionality is wrapped inside BuzzerPanel

### BuzzDTO

This is just a shared library that the desktop and web apps can use to transfer buzzer panel / pitch session state.

### BuzzBuzz

- Small, 2005-era form to:
  1. Input participant names
  2. Configure pitch session
  3. Control web UI and session flow
  4. Toggle active windows
- Creates and manages a PitchSession and sends it to the website for display
- The Form1.cs code is really just connective junk to receive input from the BuzzerPanel and convert it to the PitchSession

#### BuzzerPanel

The work here is really done in GetBuzzerNumber.  The buzzers are noisy, presumably sending no-op live handshakes constantly.  So, we smooth that out to a &#39;buzzer on&#39; indication if 1) the correct byte register and byte value byte match are found (see \_YES\_NO\_) and 2) the buzz indicates a new state.

### BuzzBuzzUI

This is a very simple website that essentially acts as a subordinate to the desktop app.  As the desktop app sends changes, the UI is updated to reflect them.  The only time that they are out of sync is when/if a user chooses to move back/forward in the Index view.

There are two screens:

- Index (/) – this is the actual pitch session that allows us to see when a sponsor has buzzed (or unbuzzed)
- Summary (/home/summary) – lists all the pitches at once with the the sponsors for each pitch

This was basically built as a bootstrap template that subscribes to an EventSource for data and binds to it using knockout.js. I even wrote the .js directly in the views (though I did go to the Herculean trouble of using the site.css file 😉).

**Note** _: BuzzBuzz expects the website to be run on port 49360, but that can be changed via the &#39;Api Uri&#39; input on the &#39;Settings&#39; tab._