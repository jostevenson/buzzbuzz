﻿using System.Collections.Generic;

namespace BuzzDTO.Models
{
    public class Idea
    {
        public string Title { get; set; }

        public string Subtitle { get; set; }

        public List<string> Sponsors { get; set; }


        public Idea()
        {
            Sponsors = new List<string>();
        }
    }
}
