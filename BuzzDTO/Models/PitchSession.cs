﻿using System.Collections.Generic;

namespace BuzzDTO.Models
{
    public class PitchSession
    {
        public int CurIdx { get; set; }

        public List<Idea> Ideas { get; set; }

        public List<string> Participants { get; set; }


        public PitchSession()
        {
            Ideas = new List<Idea>();

            Participants = new List<string>();
        }
    }
}
