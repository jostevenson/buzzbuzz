﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web.Http;

namespace BuzzBuzzUI.Controllers
{
    public class BuzzController : ApiController
    {
        //See here for EventSource utilization:
        //https://www.strathweb.com/2012/05/native-html5-push-notifications-with-asp-net-web-api-and-knockout-js/
        
        /* EventSource Subscriptions */
        protected static async Task EventSourceCallback(List<StreamWriter> subscribers, object objToStream, object semaphore)
        {
            var val_to_write = "data: " + JsonConvert.SerializeObject(objToStream) + "\n";  //"\n" because *MAGICK*

            for (int i= subscribers.Count - 1; i >= 0; i--)
            {
                try
                {
                    await subscribers[i].WriteLineAsync(val_to_write);
                    await subscribers[i].WriteLineAsync("");  //I have no idea why this seems to be required to get an immediate update
                    await subscribers[i].FlushAsync();
                }
                catch
                {
                    // This probably means the user has disconnected
                    lock (semaphore)
                    {
                        subscribers.RemoveAt(i);
                    }
                }
            }
        }
    }
}