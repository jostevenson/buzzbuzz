﻿using BuzzDTO.Models;
using System.Runtime.Caching;
using System.Web.Mvc;

namespace BuzzBuzzUI.Controllers
{
    public class HomeController : Controller
    {
        /* Action Methods */
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Summary()
        {
            return View(
                MemoryCache.Default[IdeaSessionController._KEY_SESSION_] as PitchSession
            );
        }
    }
}