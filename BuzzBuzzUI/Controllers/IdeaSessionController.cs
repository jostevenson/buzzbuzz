﻿using BuzzDTO.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Web.Http;

namespace BuzzBuzzUI.Controllers
{
    public class MoveVM
    {
        public int MoveDir { get; set; }
    }

    public class IdeaSessionController : BuzzController
    {
        /* EventSource Subscriptions */
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            var response = new HttpResponseMessage
            {
                Content = new PushStreamContent(
                    async (respStream, content, context) =>
                    {
                        var subscriber = new StreamWriter(respStream)
                        {
                            AutoFlush = true
                        };

                        lock (_PushCallbackLocker_)
                        {
                            _Subscribers.Add(subscriber);
                        }

                        await subscriber.WriteLineAsync("data: \n");
                    },
                    "text/event-stream"
                )
            };

            return response;
        }

        public static void OnStreamAvailable(Stream stream, HttpContent content, TransportContext context)
        {
            StreamWriter streamwriter = new StreamWriter(stream);
            _Subscribers.Add(streamwriter);
        }


        /* API */
        //See PUT:
        //  http://www.tutorialsteacher.com/webapi/implement-put-method-in-web-api
        [HttpPost]
        public async Task<IHttpActionResult> Put(PitchSession pitchSession)
        {
            if (pitchSession == null)
                return BadRequest("Not a valid pitch session");

            MemoryCache.Default[_KEY_SESSION_] = pitchSession;

            await EventSourceCallback(_Subscribers, pitchSession, _PushCallbackLocker_);

            return Ok();
        }

        [HttpPost]
        [Route("api/move")]
        public async Task<IHttpActionResult> Idx(MoveVM vm)
        {
            var pitchSession = MemoryCache.Default[_KEY_SESSION_] as PitchSession;
            if (pitchSession == null || pitchSession.Ideas?.Count == 0)
                return Ok();

            var cur_idx = pitchSession.CurIdx;

            var new_idx = cur_idx + vm.MoveDir;
            if (new_idx >= pitchSession.Ideas.Count)
                return Ok();

            pitchSession.CurIdx = new_idx;
            MemoryCache.Default[_KEY_SESSION_] = pitchSession;

            await EventSourceCallback(_Subscribers, pitchSession, _PushCallbackLocker_);

            return Ok();
        }

         
        /* Private Member(s) */
        public const string _KEY_SESSION_ = "PitchSession";

        protected static object _PushCallbackLocker_ = new object();

        private static readonly List<StreamWriter> _Subscribers
            = new List<StreamWriter>();
    }
}