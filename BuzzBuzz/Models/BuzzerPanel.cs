﻿using System;
using System.Collections.Generic;
using System.Media;
using System.Reflection;
using USBHIDDRIVER;

namespace BuzzBuzz.Models
{
    public class BuzzerPanel
    {
        /*** Constant(s) ***/
        private const string _BUZZER_VID_ = "vid_0079";
        private const string _BUZZER_PID_ = "pid_0006";

        private const int _BYTE_MSG_LENGTH_ = 9;
        private const int _BYTE1_ = 6;
        private const int _BYTE2_ = 7;

        private const int _MAX_BUZZERS_NUM_ = 10;


        /*** Constructor(s) ***/
        public BuzzerPanel(
            Action<int, bool> callback,
            string buzzSoundRexName = "BuzzBuzz.Zen Temple Bell-SoundBible.com-2070036999.wav",
            string buzzSoundOffRexName = "BuzzBuzz.Sad_Trombone-Joe_Lamb-665429450.wav"
        )
        {
            _Callback = callback;
            _BuzzSoundRexName = buzzSoundRexName;
            _BuzzSoundOffRexName = buzzSoundOffRexName;
        }


        /*** Public Member Function(s) ***/
        //See: http://www.florian-leitner.de/index.php/2007/08/03/hid-usb-driver-library/
        //     (found here: https://stackoverflow.com/questions/7164055/c-sharp-and-usb-hid-devices)
        public void RegisterBuzzerUnit()
        {
            if (!_Usb.Connect())
            {
                return;
            }

            SendStartCMD();

            _Usb.enableUsbBufferEvent(new EventHandler(Buzzer_OnBuzz));
            _Usb.startRead();
        }

        public void DeregisterBuzzerUnit()
        {
            try { _Usb.stopRead(); }
            catch (Exception)
            {
            }

            try { SendStopCMD(); }
            catch (Exception)
            {
            }

            try { _Usb.Disconnect(); }
            catch (Exception)
            {
            }
        }


        public void SetSuppressSound(bool suppress)
        {
            _SuppressSound = suppress;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="buzzerNum">1-based buzzer index</param>
        /// <param name="buzzerName"></param>
        public void SetParticipant(int buzzerNum, string buzzerName)
        {
            _Participants[buzzerNum - 1] = buzzerName;
        }

        public IEnumerable<string> GetParticipantNames()
        {
            int idx = 0;

            while (idx < _MAX_BUZZERS_NUM_ && !string.IsNullOrWhiteSpace(_Participants[idx]))
            {
                yield return _Participants[idx++];
            }
        }

        public IEnumerable<int> GetParticipantBuzzerNums()
        {
            int buzzerNum = 1;

            while (buzzerNum <= _MAX_BUZZERS_NUM_ && !string.IsNullOrWhiteSpace(_Participants[buzzerNum - 1]))
            {
                yield return buzzerNum++;
            }
        }

        public void ClearParticipants()
        {
            _Participants = new string[_MAX_BUZZERS_NUM_];
        }


        public void SetSponsorName(string sponsor)
        {
            for (int i = 0; i < _Participants.Length; i++)
            {
                if (string.Equals(sponsor, _Participants[i], StringComparison.OrdinalIgnoreCase))
                    _Sponsors[i + 1] = sponsor;
            }
        }

        public IEnumerable<string> GetSponsorNames()
        {
            return _Sponsors.Values;
        }

        public IEnumerable<int> GetSponsorBuzzerNums()
        {
            return _Sponsors.Keys;
        }

        public void ClearSponsors()
        {
            _Sponsors.Clear();
        }


        /*** Buzzer Unit Maintenance ***/
        private void SendStartCMD()
        {
            byte[] startCMD = new byte[8];

            //Start
            startCMD[0] = 255;
            //Mode
            startCMD[1] = 0;
            //USync
            startCMD[2] = 28;
            //ULine
            startCMD[3] = 20;
            //tSync
            startCMD[4] = 20;
            //tRepeat - High
            startCMD[5] = 0;
            //tRepeat - Low
            startCMD[6] = 0x01;
            //BusMode
            startCMD[7] = 0xF4;
            //send the command

            _Usb.write(startCMD);
        }

        private void SendStopCMD()
        {
            byte[] stopCMD = new byte[75];

            //Stop
            stopCMD[0] = 128;
            stopCMD[64] = 8;

            _Usb.write(stopCMD);
        }

        private void Buzzer_OnBuzz(object sender, EventArgs e)
        {
            var buzzerNum = GetBuzzerNumber();
            if (buzzerNum.HasValue)
            {
                var isSponsor = !_Sponsors.ContainsKey(buzzerNum.Value);

                if (isSponsor)
                {
                    //the buzzer has now become a sponsor
                    BuzzSound(on: true);

                    //Track Sponsor
                    _Sponsors[buzzerNum.Value] = _Participants[buzzerNum.Value - 1];
                }
                else
                {
                    //the buzzer is unsponsoring
                    BuzzSound(on: false);

                    //Unlist Sponsor
                    _Sponsors.Remove(buzzerNum.Value);
                }

                //Passback control to container
                _Callback(buzzerNum.Value, isSponsor);
            }
        }


        /*** Extracting Buzzer Number from USB Byte Register ***/
        private int? GetBuzzerNumber()
        {
            byte[] currentRecord = GetBuzzerInput();
            if (currentRecord?.Length < _BYTE_MSG_LENGTH_)
                return null;

            int? ret_buzzernum = null;

            //The only meaningful byte registers appear to be 6 & 7; there is no spec, 
            //  so I assume the others are active/inactive handshakes and the like
            for (int i = _BYTE1_; i <= _BYTE2_; i++)
            {
                //We want to ignore re-buzzes or buzzers that are still reporting off state
                if (_PreviouslySelected_Byte_Vals[i] != currentRecord[i])
                {
                    int buzzer = 0;
                    if ((buzzer = BuzzerNumberIfYesValue(i, currentRecord[i])) > 0)
                    {
                        ret_buzzernum = buzzer;
                    }

                    _PreviouslySelected_Byte_Vals[i] = currentRecord[i];
                }
            }

            return ret_buzzernum;
        }

        private byte[] GetBuzzerInput()
        {
            if (USBInterface.usbBuffer.Count <= 0)
                return null;

            byte[] currentRecord = null;
            int counter = 0;

            while ((byte[])USBInterface.usbBuffer[counter] == null)
            {
                //Remove this report from list
                lock (USBInterface.usbBuffer.SyncRoot)
                {
                    USBInterface.usbBuffer.RemoveAt(0);
                }
            }

            //since the remove statement at the end of the loop take the first element
            currentRecord = (byte[])USBInterface.usbBuffer[0];
            lock (USBInterface.usbBuffer.SyncRoot)
            {
                USBInterface.usbBuffer.RemoveAt(0);
            }

            return currentRecord;
        }

        private int BuzzerNumberIfYesValue(int byte_register, byte val)
        {
            for (int i = 0; i < _YES_NO_.Length; i++)
            {
                if (_YES_NO_[i].Item1 == byte_register
                    && _YES_NO_[i].Item2 == val)
                    return i + 1;
            }

            return 0;
        }

        private void BuzzSound(bool on = true)
        {
            if (_SuppressSound)
                return;

            //Plays a buzzer sound
            try
            {
                using (SoundPlayer player = new SoundPlayer(
                        //See: https://goo.gl/pWdqGC
                        Assembly.GetExecutingAssembly().GetManifestResourceStream(
                            on
                            ? _BuzzSoundRexName
                            : _BuzzSoundOffRexName
                        )
                    ))
                {
                    player.Play();
                }
            }
            catch (Exception) { }
        }


        #region _Members_
        /*** Member(s) ***/
        private bool _SuppressSound = false;

        private string[] _Participants = new string[_MAX_BUZZERS_NUM_];

        private Dictionary<int, string> _Sponsors = new Dictionary<int, string>();


        private Action<int, bool> _Callback = default(Action<int, bool>);

        private string _BuzzSoundRexName = null;

        private string _BuzzSoundOffRexName = null;


        //vid_0079 and pid_0006 are the usb buzzer device profile properties
        USBInterface _Usb = new USBInterface(_BUZZER_VID_, _BUZZER_PID_);

        private byte[] _PreviouslySelected_Byte_Vals = new byte[_BYTE_MSG_LENGTH_];

        private static Tuple<int, byte, byte>[] _YES_NO_ = new Tuple<int, byte, byte>[]
        { 
            //Tuple components:
            //  0: byte register
            //  1: YES value
            //  2: NO value
            new Tuple<int, byte, byte>(6, 0x1F, 0x0F),  //1
            new Tuple<int, byte, byte>(6, 0x2F, 0x0F),  //2
            new Tuple<int, byte, byte>(6, 0x4F, 0x0F),  //3
            new Tuple<int, byte, byte>(6, 0x8F, 0x0F),  //4
            
            new Tuple<int, byte, byte>(7, 0x01, 0x00),  //5
            new Tuple<int, byte, byte>(7, 0x02, 0x00),  //6
            new Tuple<int, byte, byte>(7, 0x04, 0x00),  //7
            new Tuple<int, byte, byte>(7, 0x08, 0x00),  //8
            
            new Tuple<int, byte, byte>(7, 0x10, 0x00),  //9
            new Tuple<int, byte, byte>(7, 0x20, 0x00)   //10
        };
        #endregion
    }
}
