﻿using BuzzBuzz.Models;
using BuzzDTO.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuzzBuzz
{
    public partial class Form1 : Form
    {
        /*** Constant(s) ***/
        private const int FOREGROUND_DELAY_MS = 7000 /*delay in ms*/;


        /*** Constructor(s) ***/
        public Form1()
        {
            InitializeComponent();

            _BuzzerPanel = new BuzzerPanel(Buzzer_OnBuzz);

            //_HttpClient.Timeout = new TimeSpan(0, 0, 0, 0, 500);
            _HttpClient.BaseAddress = new Uri(txtApiBaseUri.Text);
            _HttpClient.DefaultRequestHeaders.Accept.Clear();
            _HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }


        /*** Form Event(s) ***/
        private void Form1_Load(object sender, EventArgs e)
        {
            _BuzzerPanel.RegisterBuzzerUnit();

            //participants must be set prior to ideas
            InitParticipants();

            ParseIdeas();


            MoveIdeaSession();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);


            WriteResults();
            _BuzzerPanel.DeregisterBuzzerUnit();

            if (_HttpClient != null)
            {
                try { _HttpClient.Dispose(); } catch { }
            }
        }

        private void TextBox_Leave(object sender, EventArgs e)
        {
            _BuzzerPanel.SetParticipant(
                BuzzerNumFromTextBox((TextBox)sender),
                ((TextBox)sender).Text
            );

            _PitchSession.Participants = _BuzzerPanel.GetParticipantNames().ToList();

            BuzzApiCall();
        }

        private void TextBox_DoubleClick(object sender, EventArgs e)
        {
            //get current sponsor list
            var sponsors = (_BuzzerPanel.GetSponsorNames() ?? new List<string>()).ToList() ?? new List<string>();

            //clear them out
            _BuzzerPanel.ClearSponsors();

            //toggle sponsor selection
            bool isSponsor = false;
            var toggle_sponsor = ((TextBox)sender).Text;
            if (sponsors.Contains(toggle_sponsor))
            {
                isSponsor = false;
                sponsors.Remove(toggle_sponsor);
            }
            else
            {
                isSponsor = true;
                sponsors.Add(toggle_sponsor);
            }

            foreach (var sponsor in sponsors)
                _BuzzerPanel.SetSponsorName(sponsor);

            MoveIdeaSession();

            BuzzApiCall();
            BuzzFormUI(BuzzerNumFromTextBox(sender as TextBox), isSponsor);

            ToggleForegroundProcess();
        }

        private void Ideas_KeyUp(object sender, KeyEventArgs e)
        {
            ParseIdeas();

            MoveIdeaSession(0);

            BuzzApiCall();
        }

        private void Buzzer_OnBuzz(int buzzerNum, bool isSponsor)
        {
            MoveIdeaSession();

            BuzzApiCall();
            BuzzFormUI(buzzerNum, isSponsor);

            ToggleForegroundProcess();
        }


        private void Next_Click(object sender, EventArgs e)
        {
            MoveIdeaSession(1);
            BuzzApiCall();

            ToggleForegroundProcess();
        }

        private void Prev_Click(object sender, EventArgs e)
        {
            MoveIdeaSession(-1);
            BuzzApiCall();

            ToggleForegroundProcess();
        }

        private void Sync_Click(object sender, EventArgs e)
        {
            BuzzApiCall();

            ToggleForegroundProcess();
        }


        private void SuppressSound_CheckedChanged(object sender, EventArgs e)
        {
            _BuzzerPanel.SetSuppressSound(chkSuppressSound.Checked);
        }


        /*** Remote API Call(s) ***/
        private void BuzzApiCall()
        {
            if (chkPreventRemoteCalls.Checked)
                return;


            //async call from sync method
            Task.Run(async () => { await RemoteCallAsync(); }).Wait();
        }

        private async Task RemoteCallAsync()
        {
            HttpResponseMessage response = await _HttpClient.PostAsync(
                "api/ideasession",
                new StringContent(
                    JsonConvert.SerializeObject(_PitchSession),
                    Encoding.UTF8,
                    "application/json"
                )
            );

#if DEBUG
            Console.WriteLine(response.StatusCode);
            Console.WriteLine(response.ReasonPhrase);
            Console.WriteLine(response.Content);
#endif
        }

        private void MoveIdeaSession(int moveDir = 0)
        {
            StoreSponsors();

            var new_idx = moveDir + _PitchSession.CurIdx;

            if (new_idx < 0)
                _PitchSession.CurIdx = 0;
            else if (new_idx >= _PitchSession.Ideas.Count)
                _PitchSession.CurIdx = _PitchSession.Ideas.Count - 1;
            else
            {
                _PitchSession.CurIdx = new_idx;

                if (moveDir != 0)
                    _BuzzerPanel.ClearSponsors();

                LoadIdea();
            }

            cmdPrev.Enabled = _PitchSession.CurIdx > 0;
            cmdNext.Enabled = _PitchSession.CurIdx < _PitchSession.Ideas.Count - 1;
        }

        private void LoadIdea()
        {
            int txtIdx = 1;
            TextBox participant = null;


            Idea idea = _PitchSession.Ideas[_PitchSession.CurIdx];

            List<string>
                participants = _PitchSession.Participants,
                sponsors = idea.Sponsors;

            SetControlPropertyThreadSafe(this, "Text", idea.Title);

            if (sponsors != null && sponsors.Count > 0)
                foreach (var sponsor in sponsors)
                    _BuzzerPanel.SetSponsorName(sponsor);

            while (true)
            {
                participant = FindControl(string.Format("textBox{0}", txtIdx)) as TextBox;
                if (participant == null)
                    break;
                if (string.IsNullOrEmpty(participant.Text))
                    break;

                BuzzFormUI(
                    txtIdx,
                    sponsors != null
                    && sponsors.Count > 0
                    && sponsors.Any(O => string.Equals(O, participants[txtIdx - 1], StringComparison.OrdinalIgnoreCase))
                );

                txtIdx++;
            }
        }


        /*** Member Function(s) ***/
        private void ParseIdeas()
        {
            var ideas = txtIdeas.Text;
            if (string.IsNullOrWhiteSpace(ideas))
                return;

            int i = 0;
            var ideaAry = ideas.Split('\n');
            for (; i < ideaAry.Length; i++)
            {
                if (_PitchSession.Ideas.Count <= i)
                    _PitchSession.Ideas.Add(new Idea());

                if (ideaAry[i].Contains('~'))
                {
                    var namepcs = ideaAry[i].Split('~');
                    _PitchSession.Ideas[i].Title = namepcs[0].Trim();
                    _PitchSession.Ideas[i].Subtitle = namepcs[1].Trim();
                }
                else
                    _PitchSession.Ideas[i].Title = ideaAry[i];
            }

            //trim list
            if (_PitchSession.Ideas.Count > ideaAry.Length)
                _PitchSession.Ideas = _PitchSession.Ideas.Take(ideaAry.Length).ToList();
        }

        private void InitParticipants()
        {
            int idx = 1;

            while (true)
            {
                var txt = FindControl(string.Format("textBox{0}", idx));
                if (txt == null) break;
                if (string.IsNullOrWhiteSpace(txt.Text)) break;

                _BuzzerPanel.SetParticipant(
                    int.Parse(Regex.Replace(txt.Name, @"[^\d]", string.Empty)),
                    txt.Text
                );

                idx++;
            }

            _PitchSession.Participants = _BuzzerPanel.GetParticipantNames().ToList();
        }

        private void BuzzFormUI(int buzzerNum, bool IsSponsor)
        {
            //Highlights a buzzer's name
            SetControlPropertyThreadSafe(
                FindControl(string.Format("textBox{0}", buzzerNum)),
                "BackColor",
                IsSponsor ? Color.FromArgb(152, 251, 152) : Color.FromArgb(255, 255, 255)
            );
        }


        private void ToggleForegroundProcess()
        {
            //!!!   Only works in debug mode and is terribly brittle, make sure you don't have more than 
            //!!!   one set of chrome tabs open

            var proc = Process.GetProcessesByName("chrome").FirstOrDefault(O => O.MainWindowHandle != IntPtr.Zero);
            if (proc != null && proc.MainWindowHandle != IntPtr.Zero)
                SetForegroundWindow(proc.MainWindowHandle);

            var timer = new System.Threading.Timer(
                RestorePowerPoint,
                null,
                FOREGROUND_DELAY_MS /*delay in ms*/,
                Timeout.Infinite
            );
        }

        private void RestorePowerPoint(object state)
        {
            var proc = Process.GetProcessesByName("POWERPNT").FirstOrDefault(O => O.MainWindowHandle != IntPtr.Zero);
            if (proc != null && proc.MainWindowHandle != IntPtr.Zero)
                SetForegroundWindow(proc.MainWindowHandle);
        }

        [DllImport("user32")]
        private static extern bool SetForegroundWindow(IntPtr hwnd);


        private void StoreSponsors()
        {
            var sponsorsBuzzerNums = _BuzzerPanel.GetSponsorBuzzerNums();

            //clear current sponsors
            foreach (var buzzerNum in sponsorsBuzzerNums)
            {
                BuzzFormUI(buzzerNum, false);
            }


            //update current
            SaveCurrentIdea(_PitchSession.Ideas[_PitchSession.CurIdx]);
        }

        private void SaveCurrentIdea(Idea idea)
        {
            idea.Sponsors = _BuzzerPanel
                .GetSponsorNames()
                .ToList();
        }


        private int BuzzerNumFromTextBox(TextBox sender)
        {
            return int.Parse(Regex.Replace(
                sender.Name,
                @"[^\d]",
                ""
            ));
        }

        private void WriteResults()
        {
            StoreSponsors();

            if (_PitchSession.Ideas.Count == 0)
                return;

            File.WriteAllLines(
                Environment.ExpandEnvironmentVariables(txtResultsFileLoc.Text),
                _PitchSession.Ideas
                    .Select((O, IDX) => string.Format(
                        "{0}. {1}: {2}",
                        IDX,
                        O.Title,
                        string.Join(", ", O.Sponsors)
                    ))
            );
        }


        /*** Control Collection Helper(s) --- should be library methods ***/
        /* See: https://stackoverflow.com/questions/661561/how-do-i-update-the-gui-from-another-thread-in-c */
        private delegate void SetControlPropertyThreadSafeDelegate(
            Control control,
            string propertyName,
            object propertyValue
        );

        public static void SetControlPropertyThreadSafe(
            Control control,
            string propertyName,
            object propertyValue
        )
        {
            if (control.InvokeRequired)
            {
                control.Invoke(
                    new SetControlPropertyThreadSafeDelegate
                    (SetControlPropertyThreadSafe),
                    new object[] { control, propertyName, propertyValue }
                );

                return;
            }

            control.GetType().InvokeMember(
                propertyName,
                BindingFlags.SetProperty,
                null,
                control,
                new object[] { propertyValue }
            );
        }

        private Control FindControl(string name)
        {
            var ctls = Controls.Find(name, true);

            if (ctls == null || ctls.Length == 0)
                return null;

            return ctls[0];
        }


        /*** Member(s) ***/
        private BuzzerPanel _BuzzerPanel = null;

        private HttpClient _HttpClient = new HttpClient();

        private PitchSession _PitchSession = new PitchSession();
    }
}