﻿namespace BuzzBuzz
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabBuzzers = new System.Windows.Forms.TabPage();
            this.cmdPrev = new System.Windows.Forms.Button();
            this.cmdSync = new System.Windows.Forms.Button();
            this.chkSuppressSound = new System.Windows.Forms.CheckBox();
            this.chkPreventRemoteCalls = new System.Windows.Forms.CheckBox();
            this.cmdNext = new System.Windows.Forms.Button();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabSetup = new System.Windows.Forms.TabPage();
            this.txtIdeas = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtApiBaseUri = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtResultsFileLoc = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabBuzzers.SuspendLayout();
            this.tabSetup.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabBuzzers);
            this.tabControl1.Controls.Add(this.tabSetup);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(471, 278);
            this.tabControl1.TabIndex = 0;
            // 
            // tabBuzzers
            // 
            this.tabBuzzers.Controls.Add(this.cmdPrev);
            this.tabBuzzers.Controls.Add(this.cmdSync);
            this.tabBuzzers.Controls.Add(this.chkSuppressSound);
            this.tabBuzzers.Controls.Add(this.chkPreventRemoteCalls);
            this.tabBuzzers.Controls.Add(this.cmdNext);
            this.tabBuzzers.Controls.Add(this.textBox10);
            this.tabBuzzers.Controls.Add(this.label10);
            this.tabBuzzers.Controls.Add(this.textBox9);
            this.tabBuzzers.Controls.Add(this.label9);
            this.tabBuzzers.Controls.Add(this.textBox8);
            this.tabBuzzers.Controls.Add(this.label8);
            this.tabBuzzers.Controls.Add(this.textBox7);
            this.tabBuzzers.Controls.Add(this.label7);
            this.tabBuzzers.Controls.Add(this.textBox6);
            this.tabBuzzers.Controls.Add(this.label6);
            this.tabBuzzers.Controls.Add(this.textBox5);
            this.tabBuzzers.Controls.Add(this.label5);
            this.tabBuzzers.Controls.Add(this.textBox4);
            this.tabBuzzers.Controls.Add(this.label4);
            this.tabBuzzers.Controls.Add(this.textBox3);
            this.tabBuzzers.Controls.Add(this.label3);
            this.tabBuzzers.Controls.Add(this.textBox2);
            this.tabBuzzers.Controls.Add(this.label2);
            this.tabBuzzers.Controls.Add(this.textBox1);
            this.tabBuzzers.Controls.Add(this.label1);
            this.tabBuzzers.Location = new System.Drawing.Point(4, 22);
            this.tabBuzzers.Name = "tabBuzzers";
            this.tabBuzzers.Padding = new System.Windows.Forms.Padding(3);
            this.tabBuzzers.Size = new System.Drawing.Size(463, 252);
            this.tabBuzzers.TabIndex = 1;
            this.tabBuzzers.Text = "<Participant >";
            this.tabBuzzers.UseVisualStyleBackColor = true;
            // 
            // cmdPrev
            // 
            this.cmdPrev.Enabled = false;
            this.cmdPrev.Location = new System.Drawing.Point(22, 159);
            this.cmdPrev.Name = "cmdPrev";
            this.cmdPrev.Size = new System.Drawing.Size(75, 23);
            this.cmdPrev.TabIndex = 24;
            this.cmdPrev.Text = "<< Prev";
            this.cmdPrev.UseVisualStyleBackColor = true;
            this.cmdPrev.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Prev_Click);
            // 
            // cmdSync
            // 
            this.cmdSync.Location = new System.Drawing.Point(184, 159);
            this.cmdSync.Name = "cmdSync";
            this.cmdSync.Size = new System.Drawing.Size(75, 23);
            this.cmdSync.TabIndex = 23;
            this.cmdSync.Text = "Sync";
            this.cmdSync.UseVisualStyleBackColor = true;
            this.cmdSync.Click += new System.EventHandler(this.Sync_Click);
            // 
            // chkSuppressSound
            // 
            this.chkSuppressSound.AutoSize = true;
            this.chkSuppressSound.Location = new System.Drawing.Point(22, 220);
            this.chkSuppressSound.Name = "chkSuppressSound";
            this.chkSuppressSound.Size = new System.Drawing.Size(108, 17);
            this.chkSuppressSound.TabIndex = 22;
            this.chkSuppressSound.Text = "Suppress sound?";
            this.chkSuppressSound.UseVisualStyleBackColor = true;
            this.chkSuppressSound.CheckedChanged += new System.EventHandler(this.SuppressSound_CheckedChanged);
            // 
            // chkPreventRemoteCalls
            // 
            this.chkPreventRemoteCalls.AutoSize = true;
            this.chkPreventRemoteCalls.Location = new System.Drawing.Point(22, 197);
            this.chkPreventRemoteCalls.Name = "chkPreventRemoteCalls";
            this.chkPreventRemoteCalls.Size = new System.Drawing.Size(142, 17);
            this.chkPreventRemoteCalls.TabIndex = 21;
            this.chkPreventRemoteCalls.Text = "Prevent remote UI calls?";
            this.chkPreventRemoteCalls.UseVisualStyleBackColor = true;
            // 
            // cmdNext
            // 
            this.cmdNext.Location = new System.Drawing.Point(103, 159);
            this.cmdNext.Name = "cmdNext";
            this.cmdNext.Size = new System.Drawing.Size(75, 23);
            this.cmdNext.TabIndex = 20;
            this.cmdNext.Text = "Next >>";
            this.cmdNext.UseVisualStyleBackColor = true;
            this.cmdNext.Click += new System.EventHandler(this.Next_Click);
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.Color.White;
            this.textBox10.Location = new System.Drawing.Point(340, 37);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 20);
            this.textBox10.TabIndex = 19;
            this.textBox10.DoubleClick += new System.EventHandler(this.TextBox_DoubleClick);
            this.textBox10.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(337, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "10:";
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.Color.White;
            this.textBox9.Location = new System.Drawing.Point(234, 133);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 20);
            this.textBox9.TabIndex = 17;
            this.textBox9.DoubleClick += new System.EventHandler(this.TextBox_DoubleClick);
            this.textBox9.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(231, 116);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "9:";
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.Color.White;
            this.textBox8.Location = new System.Drawing.Point(234, 85);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 20);
            this.textBox8.TabIndex = 15;
            this.textBox8.DoubleClick += new System.EventHandler(this.TextBox_DoubleClick);
            this.textBox8.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(231, 68);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "8:";
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.White;
            this.textBox7.Location = new System.Drawing.Point(234, 37);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 13;
            this.textBox7.DoubleClick += new System.EventHandler(this.TextBox_DoubleClick);
            this.textBox7.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(231, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "7:";
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.White;
            this.textBox6.Location = new System.Drawing.Point(128, 133);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 11;
            this.textBox6.DoubleClick += new System.EventHandler(this.TextBox_DoubleClick);
            this.textBox6.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(125, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "6:";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.White;
            this.textBox5.Location = new System.Drawing.Point(128, 85);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 9;
            this.textBox5.DoubleClick += new System.EventHandler(this.TextBox_DoubleClick);
            this.textBox5.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(125, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "5:";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.White;
            this.textBox4.Location = new System.Drawing.Point(128, 37);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 7;
            this.textBox4.Text = "Tony";
            this.textBox4.DoubleClick += new System.EventHandler(this.TextBox_DoubleClick);
            this.textBox4.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(125, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "4:";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.White;
            this.textBox3.Location = new System.Drawing.Point(22, 133);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 5;
            this.textBox3.Text = "Terri";
            this.textBox3.DoubleClick += new System.EventHandler(this.TextBox_DoubleClick);
            this.textBox3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "3:";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.Location = new System.Drawing.Point(22, 85);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 3;
            this.textBox2.Text = "Harvey";
            this.textBox2.DoubleClick += new System.EventHandler(this.TextBox_DoubleClick);
            this.textBox2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "2:";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(22, 37);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "Elaine";
            this.textBox1.DoubleClick += new System.EventHandler(this.TextBox_DoubleClick);
            this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "1:";
            // 
            // tabSetup
            // 
            this.tabSetup.Controls.Add(this.txtIdeas);
            this.tabSetup.Controls.Add(this.label13);
            this.tabSetup.Controls.Add(this.txtApiBaseUri);
            this.tabSetup.Controls.Add(this.label11);
            this.tabSetup.Controls.Add(this.txtResultsFileLoc);
            this.tabSetup.Controls.Add(this.label12);
            this.tabSetup.Location = new System.Drawing.Point(4, 22);
            this.tabSetup.Name = "tabSetup";
            this.tabSetup.Size = new System.Drawing.Size(463, 252);
            this.tabSetup.TabIndex = 2;
            this.tabSetup.Text = "Setup";
            this.tabSetup.UseVisualStyleBackColor = true;
            // 
            // txtIdeas
            // 
            this.txtIdeas.Location = new System.Drawing.Point(5, 124);
            this.txtIdeas.Margin = new System.Windows.Forms.Padding(2);
            this.txtIdeas.Multiline = true;
            this.txtIdeas.Name = "txtIdeas";
            this.txtIdeas.Size = new System.Drawing.Size(442, 118);
            this.txtIdeas.TabIndex = 8;
            this.txtIdeas.Text = resources.GetString("txtIdeas.Text");
            this.txtIdeas.WordWrap = false;
            this.txtIdeas.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Ideas_KeyUp);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 107);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Ideas:";
            // 
            // txtApiBaseUri
            // 
            this.txtApiBaseUri.Location = new System.Drawing.Point(6, 75);
            this.txtApiBaseUri.Name = "txtApiBaseUri";
            this.txtApiBaseUri.Size = new System.Drawing.Size(441, 20);
            this.txtApiBaseUri.TabIndex = 5;
            this.txtApiBaseUri.Text = "http://localhost:49360/";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Api Uri:";
            // 
            // txtResultsFileLoc
            // 
            this.txtResultsFileLoc.Location = new System.Drawing.Point(6, 27);
            this.txtResultsFileLoc.Name = "txtResultsFileLoc";
            this.txtResultsFileLoc.Size = new System.Drawing.Size(441, 20);
            this.txtResultsFileLoc.TabIndex = 3;
            this.txtResultsFileLoc.Text = "%USERPROFILE%\\Desktop\\results.txt";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 10);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Results file:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 303);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Buzz, buzz!";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabBuzzers.ResumeLayout(false);
            this.tabBuzzers.PerformLayout();
            this.tabSetup.ResumeLayout(false);
            this.tabSetup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabBuzzers;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabSetup;
        private System.Windows.Forms.TextBox txtResultsFileLoc;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtApiBaseUri;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtIdeas;
        private System.Windows.Forms.Button cmdNext;
        private System.Windows.Forms.CheckBox chkPreventRemoteCalls;
        private System.Windows.Forms.CheckBox chkSuppressSound;
        private System.Windows.Forms.Button cmdSync;
        private System.Windows.Forms.Button cmdPrev;
    }
}

